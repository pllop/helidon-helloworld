package com.odigeo.architecture.helidon.bootstrap;

import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import java.lang.invoke.MethodHandles;

@ApplicationScoped
public class ApplicationInitializer {

    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass());

    public void onStartup(@Observes @Initialized(ApplicationScoped.class) Object event) {
        LOGGER.info("------------------APPLICATION BOOTSTRAP------------------");
    }
}
