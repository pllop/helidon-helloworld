package com.odigeo.architecture.helidon.kafka;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/kafka")
@RequestScoped
public class KafkaResource {

    @Inject
    KafkaController kafkaController;

    @Path("/produce/{name}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void produceToKafka(@PathParam("name") String name) {
        kafkaController.putIntoMessagingQueue(name);
    }

    @Path("/produce/pricing")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void producePricingToKafka() {
        kafkaController.publishPricing();
    }
}
