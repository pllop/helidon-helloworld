package com.odigeo.architecture.helidon.kafka.pricingmanagementpublisher;

import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.tools.notifications.NotificationMessage;
import com.odigeo.tools.notifications.action.Action;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;


public class PriceItemCacheMessagePublisher implements Runnable {

    private static final Logger logger = Logger.getLogger(PriceItemCacheMessagePublisher.class);
    private static final String PRICE_ITEM_IDS = "priceItemIds";

    private final ToolNotificationFacade toolNotificationFacade = new ToolNotificationFacade();
    private final Collection<Long> priceItemIdsList = new HashSet(Arrays.asList("a", "b", "c"));

    public static final String LOCAL_PUBLISHER_URL = "10.93.68.206:9092";
    public static final String QA_PUBLISHER_URL = "bcn1-mq-kafka-005u1.colt.edreams.com:9092,bcn1-mq-kafka-084u1.colt.edreams.com:9092,bcn1-mq-kafka-083u1.colt.edreams.com:9092";

    private void publishPriceItemCacheMessage() {
            String itemId = StringUtils.join(priceItemIdsList, ',');
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(PRICE_ITEM_IDS, itemId);

            Publisher<NotificationMessage> publisher = toolNotificationFacade.getPublisher(QA_PUBLISHER_URL);
            NotificationMessage notificationMessage = toolNotificationFacade.createNotificationMessage(Action.REFRESH_PRICE_ITEM_CACHE);
            notificationMessage.setExtraParameters(parameters);

            List<NotificationMessage> notificationList = Collections.singletonList(notificationMessage);
            try {
                publisher.publishMessages(notificationList);
                logger.info("Notification [" + Action.REFRESH_PRICE_ITEM_CACHE.name() + "] sent with price item(s): [" + parameters.get(PRICE_ITEM_IDS) + "]");

            } catch (MessageDataAccessException e) {
                logger.error("Error sending the notification to refresh PIM Tool cache with price item(s): [" + parameters.get(PRICE_ITEM_IDS) + "]", e);
            }
    }

    @Override
    public void run() {
        System.out.println("Starting Pricing Kafka Publisher Thread");
        publishPriceItemCacheMessage();
    }
}
