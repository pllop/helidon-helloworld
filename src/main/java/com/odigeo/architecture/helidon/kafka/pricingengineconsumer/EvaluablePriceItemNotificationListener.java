package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;

import com.odigeo.tools.notifications.NotificationMessage;
import com.odigeo.tools.notifications.action.Action;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;

public class EvaluablePriceItemNotificationListener extends BufferedProcessor<NotificationMessage> implements TopicNotificationListener {

    private static final Logger LOGGER = Logger.getLogger(EvaluablePriceItemNotificationListener.class);

    public EvaluablePriceItemNotificationListener(int bufferSize, long maxIntervalMilliseconds) {
        super(bufferSize, maxIntervalMilliseconds);
    }

    @Override
    public void processMessage(NotificationMessage notificationMessage) {
        if (shouldProcessAction(notificationMessage.getTopicName())) {
            addItem(notificationMessage);
        }
    }

    private boolean shouldProcessAction(String action) {
        return Action.REFRESH_PRICE_ITEM_CACHE.getTopicName().equals(action);
    }

    @Override
    protected void processBufferedItems(List<NotificationMessage> items) {
        Date notificationTimestamp = findNewestNotificationTimestamp(items);
        LOGGER.info("Kafka received - Price item cache restarted with timestamp [" + notificationTimestamp + "]");

    }

    private Date findNewestNotificationTimestamp(List<NotificationMessage> messages) {
        Date newestDate = messages.get(0).getTimestamp();
        for (int i = 1; i < messages.size(); i++) {
            if (messages.get(i).getTimestamp().after(newestDate)) {
                newestDate = messages.get(i).getTimestamp();
            }
        }
        return newestDate;
    }

    @Override
    public String getTopicName() {
        return Action.REFRESH_PRICE_ITEM_CACHE.getTopicName();
    }
}
