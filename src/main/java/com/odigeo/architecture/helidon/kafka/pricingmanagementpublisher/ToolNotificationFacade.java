package com.odigeo.architecture.helidon.kafka.pricingmanagementpublisher;

import com.odigeo.messaging.utils.Publisher;
import com.odigeo.tools.notifications.NotificationMessage;
import com.odigeo.tools.notifications.NotificationMessageFactory;
import com.odigeo.tools.notifications.action.Action;
import com.odigeo.tools.notifications.client.NotificationPublisherFactory;

public class ToolNotificationFacade {

    public Publisher<NotificationMessage> getPublisher(String url) {
        return NotificationPublisherFactory.createPublisher(url);
    }

    public NotificationMessage createNotificationMessage(Action refreshPriceItemCache) {
        return NotificationMessageFactory.createNotificationMessage(refreshPriceItemCache);
    }

}
