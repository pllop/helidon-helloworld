package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.lang.invoke.MethodHandles;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

public class NotificationGroupBuilder {

    private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass());

    private static final String GROUP_ID_SEPARATOR = "-";

    public String build() {
        return getAppName() + GROUP_ID_SEPARATOR
                + getClass().getPackage().getImplementationVersion() + GROUP_ID_SEPARATOR
                + getHostName();
    }

    private String getHostName() {
        String hostName = UUID.randomUUID().toString();
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            logger.warn("Could not resolve host name, setting randomUUID " + hostName);
        }
        return hostName;
    }

    private String getAppName() {
        String hostName = "helidon-helloworld";
        try {
            hostName = InitialContext.doLookup("java:app/AppName");
        } catch (NamingException e) {
            logger.warn("Could not resolve app name, setting randomUUID " + hostName);
        }
        return hostName;
    }
}
