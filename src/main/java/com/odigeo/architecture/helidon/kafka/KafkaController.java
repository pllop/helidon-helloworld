package com.odigeo.architecture.helidon.kafka;

import com.odigeo.architecture.helidon.kafka.pricingmanagementpublisher.PriceItemCacheMessagePublisher;
import io.helidon.messaging.connectors.kafka.KafkaMessage;
//import org.eclipse.microprofile.reactive.messaging.Incoming;
//import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.LinkedBlockingQueue;

@ApplicationScoped
public class KafkaController {

    private BlockingQueue<String> messages = new LinkedBlockingQueue<>();

    public void putIntoMessagingQueue(String message) {
        messages.add(message);
    }

//    @Incoming("from-kafka")
    public void consumeKafka(String msg) {
        System.out.println("new Helidon Kafka says: " + msg);
    }

//    @Outgoing("to-kafka")
    public CompletionStage<KafkaMessage<String, String>> send() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                String message = messages.take();
                System.out.println("Sending message to kafka with the message: " + message);
                return KafkaMessage.of("key", message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void publishPricing(){
        new PriceItemCacheMessagePublisher().run();
    }
}
