package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;

import com.odigeo.tools.notifications.client.NotificationListenersRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped
public class NotificationsControllerSubscriptionsInitializer {

    public void initSubscriptions(@Observes @Initialized(ApplicationScoped.class) Object event) {
        NotificationListenersConfiguration configuration = new NotificationListenersConfiguration();
        if (configuration.isEnabled()) {
            final Set<TopicNotificationListener> listeners = getTopicNotificationMessageListeners();

            for (TopicNotificationListener listener : listeners) {
                NotificationListenersRegistry.registerListener(NotificationListenersConfiguration.QA_CONSUMER_URL, getNotificationGroupBuilder().build(), listener.getTopicName(), listener);
                System.out.println("Initializing Pricing Engine Kafka Listener with GroupId : " + getNotificationGroupBuilder().build());
            }
        }
    }

    private static Set<TopicNotificationListener> getTopicNotificationMessageListeners() {
        Set<TopicNotificationListener> listeners = new HashSet<>();
        listeners.add(getEvaluablePriceItemListener());

        return listeners;
    }

    private static EvaluablePriceItemNotificationListener getEvaluablePriceItemListener() {
        return new EvaluablePriceItemNotificationListener(5, 6);
    }

    private NotificationGroupBuilder getNotificationGroupBuilder() {
        return new NotificationGroupBuilder();
    }
}
