package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;


import com.odigeo.tools.notifications.client.NotificationListener;

public interface TopicNotificationListener extends NotificationListener {

    String getTopicName();
}
