package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;

import com.google.common.collect.ImmutableList;
import org.apache.log4j.MDC;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * This abstract class serves as a base to implement processors that don't process items as they arrive,
 * but buffers them by size and/or time instead.
 */
@SuppressWarnings("PMD.RedundantFieldInitializer")
public abstract class BufferedProcessor<T> {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Runnable intervalExecutor;
    private ScheduledFuture<?> futureExecution = null;
    private final List<T> items;
    private final int bufferSize;
    private final long maxIntervalMilliseconds;

    protected BufferedProcessor(int bufferSize, long maxIntervalMilliseconds) {
        this.bufferSize = bufferSize;
        this.maxIntervalMilliseconds = maxIntervalMilliseconds;
        this.items = new ArrayList<>(this.bufferSize + 10);
        intervalExecutor = () -> {
            MDC.remove("traceId");
            extractAllItemsAndProcess();
        };
        scheduleMaxIntervalExecution();
    }

    protected void addItem(T item) {
        List<T> itemsExtracted = null;
        boolean mustReschedule = false;
        try {
            synchronized (items) {
                items.add(item);
                if (items.size() >= bufferSize) {
                    mustReschedule = true;
                    cancelMaxIntervalExecution();
                    itemsExtracted = extractAllItems();
                }
            }
        } finally {
            if (mustReschedule) {
                scheduleMaxIntervalExecution();
            }
        }
        if (itemsExtracted != null && !itemsExtracted.isEmpty()) {
            processBufferedItems(itemsExtracted);
        }
    }

    protected abstract void processBufferedItems(List<T> items);

    private void extractAllItemsAndProcess() {
        try {
            cancelMaxIntervalExecution();
            List<T> itemsExtracted = extractAllItems();
            if (!itemsExtracted.isEmpty()) {
                processBufferedItems(itemsExtracted);
            }
        } finally {
            scheduleMaxIntervalExecution();
        }
    }

    private void cancelMaxIntervalExecution() {
        if (futureExecution != null) {
            futureExecution.cancel(false);
        }
    }

    private List<T> extractAllItems() {
        List<T> immutableItems;
        synchronized (items) {
            immutableItems = ImmutableList.copyOf(items);
            items.clear();
        }
        return immutableItems;
    }

    private void scheduleMaxIntervalExecution() {
        if (maxIntervalMilliseconds > 0) {
            futureExecution = scheduler.schedule(intervalExecutor, maxIntervalMilliseconds, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            extractAllItemsAndProcess();
        } finally {
            super.finalize();
        }
    }
}
