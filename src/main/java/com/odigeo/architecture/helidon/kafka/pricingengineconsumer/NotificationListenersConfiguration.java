package com.odigeo.architecture.helidon.kafka.pricingengineconsumer;

import java.io.Serializable;

public class NotificationListenersConfiguration implements Serializable {

    public static final String LOCAL_CONSUMER_URL = "10.93.68.206:2181";
    public static final String QA_CONSUMER_URL = "bcn1-mq-kafka-084u1.colt.edreams.com:2181,bcn1-mq-kafka-005u1.colt.edreams.com:2181,bcn1-mq-kafka-083u1.colt.edreams.com:2181";

    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
