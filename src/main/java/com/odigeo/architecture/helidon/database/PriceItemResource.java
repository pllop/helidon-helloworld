package com.odigeo.architecture.helidon.database;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Dependent
@Path("/pricing")
public class PriceItemResource {
    @Inject
    @Named("test")
    private DataSource oracleDataSource;

    @GET
    @Path("priceItem")
    @Produces("text/plain")
    public String getTableNames() throws SQLException {
        System.out.println("in tables names");
        StringBuilder sb = new StringBuilder();
        try (Connection connection = this.oracleDataSource.getConnection();
             PreparedStatement ps =
                     connection.prepareStatement(" select * from pricing_management_app.pi_price_items");
             ResultSet rs = ps.executeQuery()) {
            if (rs.next()) {
                sb.append(rs.getString(1)).append("\n");
            }
        }
        return sb.toString();
    }
}
